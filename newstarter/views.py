from django.shortcuts import render, redirect
from django.views import generic, View
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.models import User, Group
from django.db.models import Q
from django.core.paginator import Paginator
from django.http import FileResponse
from django.core.mail import EmailMultiAlternatives, send_mail,EmailMessage

from .models import NSInfo, NSRequester, NSTeam, Status, Comments, Document
from .forms import NSInfoForm, CommentsForm, DocumentForm

import string, random

# Reference code generator
def id_generator(size=4, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


# Create your views here.

#Approval and Rejection Section
def approval_system(request, pk):
    approve_obj = NSInfo.objects.get(pk=pk)
    if approve_obj.status_id == 1:
        print('1')
        sendapprove(request, pk)
        #send_email_lmapprove(request, pk)
        return redirect('/ns/approve')
    elif approve_obj.status_id == 2:
        print('2')
        hrapproval(request, pk)
        return redirect('/ns/approve')
    elif approve_obj.status_id == 3:
        print('3')
    elif approve_obj.status_id == 4:
        print('4')
    elif approve_obj.status_id == 5:
        print('5')
    elif approve_obj.status_id == 6:
        print('6')
        sendapprove(request, pk)
        return redirect('/ns/approve')
    elif approve_obj.status_id == 7:
        print('7')
    elif approve_obj.status_id == 8:
        print('8')
        return redirect('/ns/approve')
    elif approve_obj.status_id == 9:
        print('9')
    elif approve_obj.status_id == 10:
        print('10')
        return redirect('/ns/approve')


def reject_system(request, pk):
    reject_obj = NSInfo.objects.get(pk=pk)
    if reject_obj.status_id == 1:
        return redirect('/atr/approve')
    elif reject_obj.status_id == 2:
        rejectns(request, pk)
        return redirect('/ns/approve')
    elif reject_obj.status_id == 3:
        return redirect('/ns/approve')
    elif reject_obj.status_id == 4:
        return redirect('/ns/approve')
    elif reject_obj.status_id == 5:
        return redirect('/ns/approve')
    elif reject_obj.status_id == 6:
        return redirect('/ns/approve')
    elif reject_obj.status_id == 7:
        return redirect('/ns/approve')
    elif reject_obj.status_id == 8:
        sendapprove(request, pk)
        return redirect('/ns/approve')
    elif reject_obj.status_id == 9:
        return redirect('/ns/approve')
    elif reject_obj.status_id == 10:
        hrapproval(request, pk)
        return redirect('/ns/approve')

#Functional Views
def ns_new(request):
    if request.method == "POST":
        user = request.user
        print(user)
        form = NSInfoForm(request.POST)
        if form.is_valid():
            reference = 'NS-' + str(id_generator())
            post = form.save(commit=False)
            post.author = User.objects.get(username=user)
            post.published_date = timezone.now()
            post.reference = reference
            post.status = Status.objects.get(ns_status='Draft')

            nsuser = User.objects.filter(username=user).values('id')
            nsuser = nsuser[0]
            nsuser = nsuser['id']
            requester = NSRequester.objects.get(username=nsuser)
            team = NSTeam.objects.get(id=requester.pk)
            tlm = team.line_manager
            thr = team.hr_manager
            tod = team.payroll_staff
            tb1 = team.it_staff
            post.line_manager = str(tlm)
            post.hr_manager = str(thr)
            post.payroll_person = str(tod)
            post.it_person = str(tb1)
            #upload = model_form_upload(request, pk=post.id)
            post.save()
            return redirect('nshome')

    else:
        form = NSInfoForm()
        return render(request, 'ns/ns_new.html', {'form': form})

def model_form_upload(request, pk):
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            post = form.save(commit=False)
            print(pk)
            dlink = NSInfo.objects.filter(pk=pk).values('id')
            dlink = dlink[0]
            dlink = dlink['id']
            post.documentlink = NSInfo.objects.get(pk=pk)
            post.documentlink_id = dlink
            post.save()
            return redirect('nshome')
    else:
        form = DocumentForm()
    return render(request, 'newstarter/model_form_upload.html', {'form': form})

def sendapprove(request, pk):
    sendapprove = NSInfo.objects.get(pk=pk)
    if sendapprove:
        user = request.user
        sendapprove.status = Status.objects.get(ns_status='Send for Approval')
        sendapprove.save()
        return redirect('/ns')

def hrapproval(request, pk):
    hrapproval = NSInfo.objects.get(pk=pk)
    if hrapproval:
        hrapproval.status = Status.objects.get(ns_status='HR Approval')
        hrapproval.save()
        return redirect('/ns')

def hrapprove(request, pk):
    hrapprove = NSInfo.objects.get(pk=pk)
    if hrapprove:
        hrapprove.status = Status.objects.get(ns_status='HR Approved')
        hrapprove.save()
        return redirect('/ns')

    else:
        return redirect('/ns')

def prapprove(request, pk):
    prapprove = NSInfo.objects.get(pk=pk)
    if prapprove:
        prapprove.status = Status.objects.get(ns_status='Payroll Approved')
        prapprove.save()
        return redirect('/ns')

    else:
        return redirect('/ns')

def itapprove(request, pk):
    itapprove = NSInfo.objects.get(pk=pk)
    if itapprove:
        itapprove.status = Status.objects.get(ns_status='Complete')
        itapprove.save()
        return redirect('/ns')
    else:
        return redirect('/ns')

def nscomment(request, pk):
    if request.method == "POST":
        form = CommentsForm(request.POST)
        if form.is_valid():
            user = request.user
            atrextra = NSInfo.objects.get(pk=pk)
            post = form.save(commit=False)
            post.commentlink = atrextra
            post.comment_author = user
            post.save()
            reject = rejectns(pk)
            return render(request, 'ns/ns_home.html')
    else:
        form = CommentsForm()
        return render(request, 'ns/ns_comment.html', {'form': form})

def rejectns(pk):
    rejectns = NSInfo.objects.get(pk=pk)
    if rejectns:
        rejectns.status = Status.objects.get(ns_status='Rejected')
        rejectns.save()
        return redirect('/ns')

# Class Views

class NSIndex(generic.TemplateView):
    template_name = 'ns/ns_home.html'
    context_object_name = 'ns_list'

    def get_queryset(self):
        return

class NSRequestView(generic.ListView):
    template_name = 'ns/ns_requests.html'
    context_object_name = 'ns_list'

    def get_queryset(self):
        user = self.request.user
        print(user)
        authors = User.objects.get(username=user)
        print(authors.id)
        author = authors.id
        return NSInfo.objects.order_by('-date_request').filter(author_id=author)

class NSDetailView(generic.DetailView):
    model = NSInfo
    template_name = 'ns/ns_detail.html'
    context_object_name = 'ns'

    def get_queryset(self):
        user = self.request.user
        authors = User.objects.get(username=user)
        author = authors.id
        return NSInfo.objects.order_by('-date_request').filter(author_id=author)

class NSUpdateView(generic.UpdateView):
    model = NSInfo
    fields = '__all__'
    '''
    fields = (
        'division',
        'location',
        'job_title',
        'nature_app',
        'salary',
        'pay_period',
        'benefits',
        'budget',
        'out_emp_title',
        'out_emp_salary',
        'justification',
        'advertising',
        'line_manager',
        'hr_manager',
    )
    '''
    template_name_suffix = '_update_form'

    def get_success_url(self):

        """Return the URL to redirect to after processing a valid form."""
        return str(self.success_url)  # success_url may be lazy

class NSApproveView(generic.ListView):
    template_name = 'ns/ns_approve.html'
    context_object_name = 'ns_list'

    def get_queryset(self):
        user = self.request.user
        userid = User.objects.filter(username=user).values('id')
        userid = userid[0]
        userid = userid['id']
        userid = str(userid)
        print("userid" + userid)


        try:
            it_test = NSITstaff.objects.filter(itapproverns_id=userid).values('itapproverns_id')
            print(it_test)
            it_test = it_test[0]
            it_test = it_test['itapproverns_id']
            it_test = str(it_test)
        except IndexError:
            it_test = None

        try:
            pr_test = NSPRapprover.objects.filter(prapproverns_id=userid).values('prapproverns_id')
            print(pr_test)
            pr_test = pr_test[0]
            pr_test = pr_test['prapproverns_id']
            pr_test = str(pr_test)
        except IndexError:
            pr_test = None

        try:
            hr_test = NSHRapprover.objects.filter(hrapproverns_id=userid).values('hrapproverns_id')
            print(hr_test)
            hr_test = hr_test[0]
            hr_test = hr_test['hrapproverns_id']
            hr_test = str(hr_test)
        except IndexError:
            hr_test = None

        try:
            auth_test = NSLMapprover.objects.filter(lmapproverns_id=userid).values('lmapproverns_id')
            print(auth_test)
            auth_test = auth_test[0]
            auth_test = auth_test['lmapproverns_id']
            auth_test = str(auth_test)
            print(auth_test)
        except IndexError:
            print('auth_test_except')


        '''
        if it_test == userid and auth_test == userid:
            status = NSInfo.objects.filter(Q(status__ns_status__exact='Send for Approval') & Q(lmapproverns_id=userid) | Q(status__atr_status__exact='Payroll Approved') & Q(itapproverns_id=userid))
            return status.order_by('-date_request')
        '''
        if it_test == userid:
            status = NSInfo.objects.filter(status__ns_status__exact='Payroll Approved')
            print(status)
            return status.order_by('-date_request')
        elif pr_test == userid:
            status = NSInfo.objects.filter(status__ns_status__exact='HR Approved')
            return status.order_by('-date_request')
        elif hr_test == userid:
            status = NSInfo.objects.filter(status__ns_status__exact='HR Approval')
            return status.order_by('-date_request')
        elif auth_test == userid:
            print('Matcb Auth test')
            status = NSInfo.objects.filter(status__ns_status__exact='Send for Approval')
            print(status)
            return status.order_by('-date_request')

class NSApproveDetailView(generic.DetailView):
    model = NSInfo
    template_name = 'ns/ns_approvedetail.html'
    context_object_name = 'ns'


class NSComments(generic.ListView):
    model = Comments
    template_name = 'ns/comment_details.html'
    context_object_name = 'com_list'


    def get_queryset(self, *args, **kwarg):
        pk = self.kwargs.get('pk')
        ns = NSInfo.objects.filter(id=pk).values('id')
        ns = ns[0]
        ns = ns['id']
        if ns:
            try:
                comments = Comments.objects.order_by('comment').filter(commentlink_id=ns)
                return comments
            except:
                #Comments.objects.order_by('comment').filter(commentlink=ns)
                pass

class NSHRView(generic.ListView):
    template_name = 'ns/ns_hrview.html'
    context_object_name = 'ns_list'
    paginate_by = 10

    def get_queryset(self):
        return NSInfo.objects.order_by('-date_request')

class NSHRDetailView(generic.DetailView):
    model = NSInfo
    template_name = 'ns/ns_hrdetail.html'
    context_object_name = 'ns'

    def get_queryset(self):
        return NSInfo.objects.order_by('-date_request')