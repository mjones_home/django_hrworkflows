from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User, Group
from multiselectfield import MultiSelectField

# Create your models here.
class Status(models.Model):
    ns_status = models.CharField(max_length=30)
    ns_status_stage = models.IntegerField(null=True)

    def __str__(self):
        return self.ns_status

class NSTeam(models.Model):
    team_name = models.CharField(max_length=50)
    line_manager = models.CharField(max_length=50, null=True)
    hr_manager = models.CharField(max_length=50, null=True)
    payroll_person = models.CharField(max_length=50, null=True)
    it_person = models.CharField(max_length=50, null=True)

    def __str__(self):
        return self.team_name

class NSRequester(models.Model):
    nsusername = models.ForeignKey(User, related_name='nsuser', on_delete=models.CASCADE)
    nsfirst_name = models.CharField(max_length=50)
    nslast_name = models.CharField(max_length=50)
    nsemail = models.CharField(max_length=50)
    nsteam = models.ForeignKey(NSTeam, related_name='nsrequest', on_delete=models.CASCADE)

    def __str__(self):
        return self.nsemail

class NSInfo(models.Model):
    MR = 'MR'
    MRS = 'MRS'
    MS = 'MS'
    MISS = 'MISS'
    DR = 'DR'

    TITLE = (
        (MR, 'MR'),
        (MRS, 'MRS'),
        (MS, 'MS'),
        (MISS, 'MISS'),
        (DR, 'DR'),
        )


    AFC = 'AFC'
    NAFC = 'Non-AFC'
    M = 'Male'
    F = 'Female'

    GRADE_TYPE = (
        (AFC,'AFC'),
        (NAFC, 'Non-AFC'),

    )

    GENDER = (
        (M, 'Male'),
        (F, 'Female'),

    )

    C1 = 'Linen Non-AFC zero hour Contract'
    C2 = 'Linen Non-AFC Temporary Contract'
    C3 = 'Linen Non-AFC Permanent Contract'
    C4 = 'Linen Non-AFC Permanent Drivers Contract'
    C5 = 'Linen Non-AFC Temp Drivers Contract'

    CSTANDARD = (
        (C1, 'Linen Non-AFC zero hour Contract'),
        (C2, 'Linen Non-AFC Temporary Contract'),
        (C3, 'Linen Non-AFC Permanent Contract'),
        (C4, 'Linen Non-AFC Permanent Drivers Contract'),
        (C5, 'Linen Non-AFC Temp Drivers Contract'),
    )

    CT1 = 'Permenant'
    CT2 = 'Temporary'

    CTYPE = (
        (CT1, 'Permenant'),
        (CT2, 'Temporary'),
    )

    EM = 'Email'
    OFF = 'Office'
    TA = 'Time and Attendance'
    ABS = 'ABSolute'
    BAR = 'Barcellos'
    SAGE = 'Sage 200'
    SAGEPR = 'Sage PR'
    APPOGEE = 'Appogee'
    RDP1 = 'Remote Services'
    VPN = 'VPN'
    VOIP = '3CX VOIP'

    ITSERVICES_CHOICE = (
        (EM, 'Email'),
        (OFF, 'Office'),
        (TA, 'Time and Attendance'),
        (ABS, 'ABSolute'),
        (BAR, 'Barcellos'),
        (SAGE, 'Sage 200'),
        (SAGEPR, 'Sage PR'),
        (APPOGEE, 'Appogee'),
        (RDP1, 'Remote Services'),
        (VPN, 'VPN'),
        (VOIP, '3CX VOIP'),
    )

    DESKTOP = 'Desktop'
    LAPTOP = 'Laptop'
    DPHONE = 'Desk Phone'
    MPHONE = 'Mobile Phone'
    DOCK = 'Docking Station'
    MON = 'Monitor'
    PRIN = 'Printer'
    TABLET = 'Tablet'

    ITHW_CHOICE = (
        (DESKTOP, 'Desktop'),
        (LAPTOP, 'Laptop'),
        (DPHONE, 'Desk Phone'),
        (MPHONE, 'Mobile Phone'),
        (DOCK, 'Docking Station'),
        (MON, 'Monitor'),
        (PRIN, 'Printer'),
        (TABLET, 'Tablet'),

    )


    reference = models.SlugField(max_length=10, unique=True)
    author = models.ForeignKey(NSRequester, related_name='nsinfo', on_delete=models.CASCADE)
    date_request = models.DateTimeField(auto_now_add=True)
    status = models.ForeignKey(Status, related_name='nsinfo', on_delete=models.CASCADE)
    title = models.CharField(max_length=30, null=True, choices=TITLE)
    first_name = models.CharField(max_length=30, null=True)
    last_name = models.CharField(max_length=30, null=True)
    gender = models.CharField(max_length=30, null=True, choices=GENDER)
    dob = models.DateField(null=True)
    nationality  = models.CharField(max_length=15, null=True)
    address = models.TextField(max_length=600, null=True)
    city  = models.CharField(max_length=30, null=True)
    postcode = models.CharField(max_length=10, null=True)
    start_date = models.DateField(null=True)
    job_title = models.CharField(max_length=30, null=True)
    location = models.CharField(max_length=30, null=True)
    department  = models.CharField(max_length=30, null=True)
    cost_centre	 = models.CharField(max_length=30, null=True)
    grade_type  = models.CharField(max_length=30, null=True, choices=GRADE_TYPE, default='NAFC')
    probation_end_date = models.DateField(null=True)
    probation_contact = models.CharField(max_length=30, null=True)
    holiday_year = models.CharField(max_length=30, null=True)
    holiday_allowance = models.IntegerField(default=0)
    entitlements = models.CharField(max_length=30, null=True)
    payroll_period = models.CharField(max_length=30, null=True)
    basic_salary = models.IntegerField(default=0)
    working_hours = models.IntegerField(default=0)
    work_patterns = models.TextField(max_length=600, null=True)
    contract_standard = models.CharField(max_length=30, null=True, choices=CSTANDARD)
    contract_type = models.CharField(max_length=30, null=True, choices=CTYPE)
    nscomments  = models.TextField(max_length=600, null=True)
    it_services = MultiSelectField(choices=ITSERVICES_CHOICE)
    it_hardware = MultiSelectField(choices=ITHW_CHOICE)
    line_manager = models.CharField(max_length=50,null=True)
    it_staff = models.CharField(max_length=50,null=True)
    hr_manager = models.CharField(max_length=50,null=True)
    payroll_staff = models.CharField(max_length=50,null=True)

    def __str__(self):
        return str(self.reference)

class Comments(models.Model):
    commentlink = models.ForeignKey(NSInfo, related_name='comment', on_delete=models.CASCADE)
    comment = models.TextField(max_length=500)
    comment_author = models.CharField(max_length=50)

    def __str__(self):
        return self.comment

class Document(models.Model):
    documentlink = models.ForeignKey(NSInfo, related_name='document', on_delete=models.CASCADE)
    description = models.CharField(max_length=255, blank=True)
    document = models.FileField(upload_to='documents/')
    uploaded_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.description

