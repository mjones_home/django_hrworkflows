from django.apps import AppConfig


class NewstarterConfig(AppConfig):
    name = 'newstarter'
