from django.contrib import admin
from newstarter.models import NSInfo, NSRequester, NSTeam, Status

# Register your models here.

class NSInfoAdmin(admin.ModelAdmin):
    list_display = ('reference', 'job_title','status','author')
    list_filter = ['reference']
    search_fields = ['author']

admin.site.register(NSInfo, NSInfoAdmin)

class NSRequesterAdmin(admin.ModelAdmin):
    list_display = ('nsusername', 'nsemail')
    list_filter = ['nsusername']
    search_fields = ['nsusername']

admin.site.register(NSRequester, NSRequesterAdmin)

class NSTeamAdmin(admin.ModelAdmin):
    list_display = ['team_name']
    list_filter = ['team_name']
    search_fields = ['team_name']

admin.site.register(NSTeam, NSTeamAdmin)

class StatusAdmin(admin.ModelAdmin):
    list_display = ('ns_status', 'ns_status_stage')
    list_filter = ['ns_status']
    search_fields = ['ns_status']

admin.site.register(Status, StatusAdmin)