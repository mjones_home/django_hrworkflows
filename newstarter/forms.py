from django.forms import ModelForm, SelectDateWidget
from django.utils.translation import gettext_lazy as _

from .models import NSInfo, Comments, Document

class NSInfoForm(ModelForm):
    class Meta:
        model = NSInfo
        fields = '__all__'
        exclude = ('reference',
                   'status',
                   'author',
                   'line_manager',
                   'hr_manager',
                   'payroll_staff',
                   'it_staff',
                   )
        DOY = ('1950', '1951', '1952', '1953', '1954', '1955', '1956', '1957', '1958', '1959',
                '1960', '1961', '1962', '1963', '1964', '1965', '1966', '1967', '1968', '1969',
                '1970', '1971', '1972', '1973', '1974', '1975', '1976', '1977', '1978', '1979',
                '1980', '1981', '1982', '1983', '1984', '1985', '1986', '1987',
               '1988', '1989', '1990', '1991', '1992', '1993', '1994', '1995',
               '1996', '1997', '1998', '1999', '2000', '2001', '2002', '2003',
               '2004', '2005', '2006', '2007', '2008', '2009', '2010', '2011',
               '2012', '2013', '2014', '2015', '2016', '2017', '2018', '2019', '2020',
               '2021', '2022', '2023', '2024', '2025', '2026', '2027', '2028')

        DOY1 = ('2018', '2019', '2020', '2021', '2022', '2023', '2024', '2025', '2026', '2027', '2028')

        widgets = {
            'dob': SelectDateWidget(years=DOY),
            'start_date': SelectDateWidget(years=DOY1),
            'probation_end_date': SelectDateWidget(years=DOY1),

        }
        #labels = {
         #   'nature_app' : _('Nature of Application'),
          #  'out_emp_title': _('Outgoing Employee Title'),
           # 'out_emp_salary': _('Outgoing Employee Salary'),
            #      }

class CommentsForm(ModelForm):

    class Meta:
        model = Comments
        fields = ('comment',)

class DocumentForm(ModelForm):
    class Meta:
        model = Document
        fields = ('description', 'document', )
