from django.contrib import admin
from django.urls import path, re_path, include
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView
from hrworkflowapp import views as hrviews
from newstarter import views as nsviews
from leavers import views as lviews
from changes import views as chgviews

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', login_required(TemplateView.as_view(template_name='hrw/index.html')), name="index"),
    path('accounts/', include('django.contrib.auth.urls'), name='accounts'),
    path('import_user/',hrviews.simple_upload_user, name='import_user'),
    path('import_team/',hrviews.simple_upload_team, name='import_team'),
    path('import_atrr/',hrviews.simple_upload_atrr, name='import_atrr'),
    path('atr/',login_required(hrviews.ATRIndexView.as_view()), name='atrhome'),
    path('atr/request/', login_required(hrviews.ATRRequestView.as_view()), name='atrrequest'),
    path('atr/norequests', hrviews.NOATRIndexView.as_view(), name='norequests'),
    path('atr/approve/', login_required(hrviews.ATRApproveView.as_view()), name='atrapprove'),
    path('atr/hr/', hrviews.ATRHRView.as_view(), name='atrhrview'),
    re_path(r'^atr/request/(?P<pk>[0-9]+)/$',hrviews.ATRDetailView.as_view(),name='atrdetail'),
    re_path(r'^atr/approve/(?P<pk>[0-9]+)/$',hrviews.ATRApproveDetailView.as_view(),name='atrappdetail'),
    re_path(r'^atr/appsys/(?P<pk>[0-9]+)/$', hrviews.approval_system, name='approvalsys'),
    re_path(r'^atr/rejectsys/(?P<pk>[0-9]+)/$', hrviews.reject_system, name='rejectsys'),
    re_path(r'^atr/deletesys/(?P<pk>[0-9]+)/$', hrviews.delete_obj, name='deletesys'),
    re_path(r'atr/update/(?P<pk>[0-9]+)/$',hrviews.ATRUpdateView.as_view(success_url='/atr'),name='atrupdate'),
    re_path(r'^atr/hr/(?P<pk>[0-9]+)/$',hrviews.ATRHRDetailView.as_view(),name='atrhrviewdetails'),
    re_path(r'^sendapprove/(?P<pk>[0-9]+)/$', hrviews.sendapprove, name='sendapprove'),
    re_path(r'^hrapprove/(?P<pk>[0-9]+)/$',hrviews.hrapprove, name='hrapprove'),
    re_path(r'^bapprove/(?P<pk>[0-9]+)/$',hrviews.boardapprove, name='boardapprove'),
    re_path(r'^reject/(?P<pk>[0-9]+)/$',hrviews.rejectatr, name='rejectatr'),
    path('atr/new', login_required(hrviews.atr_new), name='atr_new'),
    #path('atr/comment', hrviews.atr_comment, name='atrcomment'),
    #re_path(r'^atr/comment/(?P<pk>[0-9]+)/$',hrviews.atr_comment, name='atrcommentpk'),
    #path('atr/comments/', hrviews.ATRComments.as_view(), name='atrcomments'),
    #re_path(r'^atr/comments/(?P<pk>[0-9]+)/$',hrviews.ATRComments.as_view(),name='atrcomments1'),
    path('atr/upload', login_required(hrviews.model_form_upload), name='upload'),
    re_path(r'^atr/upload/(?P<pk>[0-9]+)/$',hrviews.model_form_upload,name='atrupload'),
    path('atr/documents/', login_required(hrviews.ATRDocumentsView.as_view()), name='atrdocuments'),
    re_path(r'^atr/document/(?P<pk>[0-9]+)/$',hrviews.ATRDocumentsView.as_view(),name='atrdoc'),
    re_path(r'^atr/download/(?P<pk>[0-9]+)/$',hrviews.download, name='atrdownload'),
    path('atr/profile',login_required(hrviews.ATRProfileView.as_view()), name='atrprofile'),
    # Newstarter URLS
    path('ns/',login_required(nsviews.NSIndex.as_view()), name='nshome'),
    path('ns/new', nsviews.ns_new, name='ns_new'),
    re_path(r'^ns/upload/(?P<pk>[0-9]+)/$',nsviews.model_form_upload,name='nsupload'),
    path('ns/request/', nsviews.NSRequestView.as_view(), name='nsrequest'),
    re_path(r'^ns/request/(?P<pk>[0-9]+)/$',nsviews.NSDetailView.as_view(),name='nsdetail'),
    re_path(r'ns/update/(?P<pk>[0-9]+)/$',nsviews.NSUpdateView.as_view(success_url='/ns'),name='nsupdate'),
    path('ns/approve/', nsviews.NSApproveView.as_view(), name='nsapprove'),
    re_path(r'^ns/sendapprove/(?P<pk>[0-9]+)/$', nsviews.sendapprove, name='nssendapprove'),
    re_path(r'^ns/hrapproval/(?P<pk>[0-9]+)/$',nsviews.hrapproval, name='nshrapproval'),
    re_path(r'^ns/hrapprove/(?P<pk>[0-9]+)/$',nsviews.hrapprove, name='nshrapprove'),
    re_path(r'^ns/prapprove/(?P<pk>[0-9]+)/$',nsviews.prapprove, name='nsprapprove'),
    re_path(r'^ns/itapprove/(?P<pk>[0-9]+)/$',nsviews.itapprove, name='nsitapprove'),
    re_path(r'^ns/approve/(?P<pk>[0-9]+)/$',nsviews.NSApproveDetailView.as_view(),name='nsappdetail'),
    re_path(r'^ns/comment/(?P<pk>[0-9]+)/$',nsviews.NSComments.as_view(), name='nscomment'),
    path('ns/comments/', nsviews.NSComments.as_view(), name='nscomments'),
    re_path(r'^ns/reject/(?P<pk>[0-9]+)/$',nsviews.rejectns, name='rejectns'),
    path('ns/hr/', nsviews.NSHRView.as_view(), name='nshrview'),
    re_path(r'^ns/hr/(?P<pk>[0-9]+)/$',nsviews.NSHRDetailView.as_view(),name='nshrviewdetails'),
    # Leavers URLS
    path('lvr/',login_required(lviews.LVRIndex.as_view()), name='lvrhome'),
    path('lvr/new', lviews.lvr_new, name='lvr_new'),
    path('lvr/request/', lviews.LVRRequestView.as_view(), name='lvrrequest'),
    re_path(r'^lvr/request/(?P<pk>[0-9]+)/$',lviews.LVRDetailView.as_view(),name='lvrdetail'),
    re_path(r'lvr/update/(?P<pk>[0-9]+)/$',lviews.LVRUpdateView.as_view(success_url='/ns'),name='lvrupdate'),
    path('lvr/approve/', lviews.LVRApproveView.as_view(), name='lvrapprove'),
    re_path(r'^lvr/sendapprove/(?P<pk>[0-9]+)/$', lviews.sendapprove, name='lvrsendapprove'),
    re_path(r'^lvr/hrapproval/(?P<pk>[0-9]+)/$',lviews.hrapproval, name='lvrhrapproval'),
    re_path(r'^lvr/hrapprove/(?P<pk>[0-9]+)/$',lviews.hrapprove, name='lvrhrapprove'),
    re_path(r'^lvr/prapprove/(?P<pk>[0-9]+)/$',lviews.prapprove, name='lvrprapprove'),
    re_path(r'^lvr/itapprove/(?P<pk>[0-9]+)/$',lviews.itapprove, name='lvritapprove'),
    re_path(r'^lvr/approve/(?P<pk>[0-9]+)/$',lviews.LVRApproveDetailView.as_view(),name='lvrappdetail'),
    #re_path(r'^lvr/comment/(?P<pk>[0-9]+)/$',lviews.lvrcomment, name='lvrcomment'),
    path('lvr/comment/', lviews.lvrcomment, name='lvrcomment'),
    re_path(r'^lvr/comments/(?P<pk>[0-9]+)/$',lviews.LVRComments.as_view(), name='lvrcomments'),
    re_path(r'^lvr/reject/(?P<pk>[0-9]+)/$',lviews.rejectlvr, name='rejectlvr'),
    path('lvr/hr/', lviews.LVRHRView.as_view(), name='lvrhrview'),
    re_path(r'^lvr/hr/(?P<pk>[0-9]+)/$',lviews.LVRHRDetailView.as_view(),name='lvrhrviewdetails'),
    # Changes
    path('chg/',login_required(chgviews.CHGIndex.as_view()), name='chghome'),
    path('chg/new', chgviews.chg_new, name='chg_new'),
    path('chg/request/', chgviews.CHGRequestView.as_view(), name='chgrequest'),
    path('chg/approve/', chgviews.CHGApproveView.as_view(), name='chgapprove'),
    path('chg/hr/', chgviews.CHGHRView.as_view(), name='chghrview'),


    # TEST AREA


    ]

