from django.contrib import admin
from leavers.models import LVRModel,LVRPRapprover,LVRLMapprover,LVRITstaff,LVRHRapprover,Status

# Register your models here.
class LVRModelAdmin(admin.ModelAdmin):
    list_display = ('reference','status','author')
    list_filter = ['reference']
    search_fields = ['author']

admin.site.register(LVRModel, LVRModelAdmin)

class LVRLMapproverAdmin(admin.ModelAdmin):
    list_display = ('lmapproverlvr', 'email','username1')
    list_filter = ['username1']
    search_fields = ['username1']

admin.site.register(LVRLMapprover, LVRLMapproverAdmin)

class LVRITstaffAdmin(admin.ModelAdmin):
    list_display = ('itapproverlvr', 'email','username1')
    list_filter = ['username1']
    search_fields = ['username1']

admin.site.register(LVRITstaff, LVRITstaffAdmin)

class LVRPRapproverAdmin(admin.ModelAdmin):
    list_display = ('prapproverlvr', 'email','username1')
    list_filter = ['username1']
    search_fields = ['username1']

admin.site.register(LVRPRapprover, LVRPRapproverAdmin)

class LVRHRapproverAdmin(admin.ModelAdmin):
    list_display = ('hrapproverlvr', 'email','username1')
    list_filter = ['username1']
    search_fields = ['username1']

admin.site.register(LVRHRapprover, LVRHRapproverAdmin)


class StatusAdmin(admin.ModelAdmin):
    list_display = ('lvr_status', 'lvr_status_description')
    list_filter = ['lvr_status']
    search_fields = ['lvr_status']

admin.site.register(Status, StatusAdmin)