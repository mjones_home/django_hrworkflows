from django.shortcuts import render, redirect
from django.views import generic, View
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.models import User, Group
from django.db.models import Q
from django.core.paginator import Paginator
from django.http import FileResponse
from django.core.mail import EmailMultiAlternatives, send_mail,EmailMessage

from .models import LVRModel,LVRHRapprover,LVRITstaff,LVRLMapprover,LVRPRapprover, Status, Comments, Document
from .forms import LVRForm, CommentsForm, DocumentForm

import string, random

# Reference code generator
def id_generator(size=4, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))
# Create your views here.

# Functional Views
def lvr_new(request):
    if request.method == "POST":
        user = request.user
        print(user)
        form = LVRForm(request.POST)
        if form.is_valid():
            reference = 'LVR-' + str(id_generator())
            print(reference)
            post = form.save(commit=False)
            post.author = User.objects.get(username=user)
            post.published_date = timezone.now()
            post.reference = reference
            post.status = Status.objects.get(lvr_status='Draft')
            print(post.status)
            #upload = model_form_upload(request, pk=post.id)
            post.save()
            return redirect('lvrhome')

    else:
        form = LVRForm()
        return render(request, 'leavers/lvr_new.html', {'form': form})

def sendapprove(request, pk):
    sendapprove = LVRModel.objects.get(pk=pk)
    if sendapprove:
        user = request.user
        sendapprove.status = Status.objects.get(lvr_status='Send for Approval')
        sendapprove.save()
        subject, from_email, to = 'NS - Line Manager Approval Needed', 'hrworkflows@synergylms.co.uk', user.email
        text_content = 'This is an important message.'
        html_content = '<p>This is an <strong>important</strong> message.</p>'
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        return redirect('/lvr')

def hrapproval(request, pk):
    hrapproval = LVRModel.objects.get(pk=pk)
    if hrapproval:
        hrapproval.status = Status.objects.get(lvr_status='HR Approval')
        hrapproval.save()
        return redirect('/lvr')

def hrapprove(request, pk):
    hrapprove = LVRModel.objects.get(pk=pk)
    if hrapprove:
        hrapprove.status = Status.objects.get(lvr_status='HR Approved')
        hrapprove.save()
        return redirect('/lvr')

    else:
        return redirect('/lvr')

def prapprove(request, pk):
    prapprove = LVRModel.objects.get(pk=pk)
    if prapprove:
        prapprove.status = Status.objects.get(lvr_status='Payroll Approved')
        prapprove.save()
        return redirect('/lvr')

    else:
        return redirect('/ns')

def itapprove(request, pk):
    itapprove = LVRModel.objects.get(pk=pk)
    if itapprove:
        itapprove.status = Status.objects.get(lvr_status='Complete')
        itapprove.save()
        return redirect('/lvr')
    else:
        return redirect('/lvr')

def lvrcomment(request, pk):
    if request.method == "POST":
        form = CommentsForm(request.POST)
        if form.is_valid():
            user = request.user
            atrextra = LVRModel.objects.get(pk=pk)
            post = form.save(commit=False)
            post.commentlink = atrextra
            post.comment_author = user
            post.save()
            reject = rejectns(pk)
            return render(request, 'leavers/lvr_home.html')
    else:
        form = CommentsForm()
        return render(request, 'leavers/lvr_comment.html', {'form': form})

def rejectlvr(pk):
    rejectlvr = LVRModel.objects.get(pk=pk)
    if rejectlvr:
        rejectlvr.status = Status.objects.get(lvr_status='Rejected')
        rejectlvr.save()
        return redirect('/lvr')


# Class Views
class LVRIndex(generic.TemplateView):
    template_name = 'leavers/lvr_home.html'
    context_object_name = 'lvr_list'

    def get_queryset(self):
        return

class LVRRequestView(generic.ListView):
    template_name = 'leavers/lvr_requests.html'
    context_object_name = 'lvr_list'

    def get_queryset(self):
        user = self.request.user
        print(user)
        authors = User.objects.get(username=user)
        print(authors.id)
        author = authors.id
        return LVRModel.objects.order_by('-date_request').filter(author_id=author)

class LVRDetailView(generic.DetailView):
    model = LVRModel
    template_name = 'leavers/lvr_detail.html'
    context_object_name = 'lvr'

    def get_queryset(self):
        user = self.request.user
        authors = User.objects.get(username=user)
        author = authors.id
        return LVRModel.objects.order_by('-date_request').filter(author_id=author)

class LVRUpdateView(generic.UpdateView):
    model = LVRModel
    fields = '__all__'
    '''
    fields = (
        'division',
        'location',
        'job_title',
        'nature_app',
        'salary',
        'pay_period',
        'benefits',
        'budget',
        'out_emp_title',
        'out_emp_salary',
        'justification',
        'advertising',
        'line_manager',
        'hr_manager',
    )
    '''
    template_name_suffix = '_update_form'

    def get_success_url(self):

        """Return the URL to redirect to after processing a valid form."""
        return str(self.success_url)  # success_url may be lazy

class LVRApproveView(generic.ListView):
    template_name = 'leavers/lvr_approve.html'
    context_object_name = 'lvr_list'

    def get_queryset(self):
        user = self.request.user
        userid = User.objects.filter(username=user).values('id')
        userid = userid[0]
        userid = userid['id']
        userid = str(userid)
        print("userid" + userid)


        try:
            it_test = LVRITstaff.objects.filter(itapproverlvr_id=userid).values('itapproverlvr_id')
            print(it_test)
            it_test = it_test[0]
            it_test = it_test['itapproverlvr_id']
            it_test = str(it_test)
        except IndexError:
            it_test = None

        try:
            pr_test = LVRPRapprover.objects.filter(prapproverlvr_id=userid).values('prapproverlvr_id')
            print(pr_test)
            pr_test = pr_test[0]
            pr_test = pr_test['prapproverlvr_id']
            pr_test = str(pr_test)
        except IndexError:
            pr_test = None

        try:
            hr_test = LVRHRapprover.objects.filter(hrapproverlvr_id=userid).values('hrapproverlvr_id')
            print(hr_test)
            hr_test = hr_test[0]
            hr_test = hr_test['hrapproverlvr_id']
            hr_test = str(hr_test)
        except IndexError:
            hr_test = None

        try:
            auth_test = LVRLMapprover.objects.filter(lmapproverlvr_id=userid).values('lmapproverlvr_id')
            print(auth_test)
            auth_test = auth_test[0]
            auth_test = auth_test['lmapproverlvr_id']
            auth_test = str(auth_test)
            print(auth_test)
        except IndexError:
            print('auth_test_except')

        if it_test == userid:
            status = LVRModel.objects.filter(status__lvr_status__exact='Payroll Approved')
            print(status)
            return status.order_by('-date_request')
        elif pr_test == userid:
            status = LVRModel.objects.filter(status__lvr_status__exact='HR Approved')
            return status.order_by('-date_request')
        elif hr_test == userid:
            status = LVRModel.objects.filter(status__lvr_status__exact='HR Approval')
            return status.order_by('-date_request')
        elif auth_test == userid:
            print('Matcb Auth test')
            status = LVRModel.objects.filter(status__lvr_status__exact='Send for Approval')
            print(status)
            return status.order_by('-date_request')

class LVRApproveDetailView(generic.DetailView):
    model = LVRModel
    template_name = 'leavers/lvr_approvedetail.html'
    context_object_name = 'lvr'

class LVRComments(generic.ListView):
    model = Comments
    template_name = 'leavers/comment_details.html'
    context_object_name = 'com_list'


    def get_queryset(self, *args, **kwarg):
        pk = self.kwargs.get('pk')
        lvr = LVRModel.objects.filter(id=pk).values('id')
        lvr = lvr[0]
        lvr = lvr['id']
        if lvr:
            try:
                comments = Comments.objects.order_by('comment').filter(commentlink_id=lvr)
                return comments
            except:
                #Comments.objects.order_by('comment').filter(commentlink=atr)
                pass

class LVRHRView(generic.ListView):
    template_name = 'leavers/lvr_hrview.html'
    context_object_name = 'lvr_list'
    paginate_by = 10

    def get_queryset(self):
        return LVRModel.objects.order_by('-date_request')

class LVRHRDetailView(generic.DetailView):
    model = LVRModel
    template_name = 'leavers/lvr_hrdetail.html'
    context_object_name = 'lvr'

    def get_queryset(self):
        return LVRModel.objects.order_by('-date_request')