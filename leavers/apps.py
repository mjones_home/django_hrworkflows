from django.apps import AppConfig


class LeaversConfig(AppConfig):
    name = 'leavers'
