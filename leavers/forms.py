from django.forms import ModelForm, SelectDateWidget, DateInput
from django.utils.translation import gettext_lazy as _

from .models import LVRModel, Comments, Document

class LVRForm(ModelForm):
    class Meta:
        model = LVRModel
        fields = '__all__'
        exclude = ('reference',
                   'status',
                   'author',
                   )

        DOY = ('1980', '1981', '1982', '1983', '1984', '1985', '1986', '1987',
               '1988', '1989', '1990', '1991', '1992', '1993', '1994', '1995',
               '1996', '1997', '1998', '1999', '2000', '2001', '2002', '2003',
               '2004', '2005', '2006', '2007', '2008', '2009', '2010', '2011',
               '2012', '2013', '2014', '2015','2016','2017','2018','2019','2020',
               '2021','2022','2023','2024','2025','2026','2027','2028')

        DOY1 = ('2018','2019','2020','2021','2022','2023','2024','2025','2026','2027','2028')

        widgets = {
            'leaver_date': SelectDateWidget(years=DOY1),
            'position_start_date': SelectDateWidget(years=DOY),
            'last_working_day': SelectDateWidget(years=DOY1),

        }
        #labels = {
         #   'nature_app' : _('Nature of Application'),
          #  'out_emp_title': _('Outgoing Employee Title'),
           # 'out_emp_salary': _('Outgoing Employee Salary'),
            #      }

class CommentsForm(ModelForm):

    class Meta:
        model = Comments
        fields = ('comment',)

class DocumentForm(ModelForm):
    class Meta:
        model = Document
        fields = ('description', 'document', )
