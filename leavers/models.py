from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User, Group

# Create your models here.
class Status(models.Model):
    lvr_status = models.CharField(max_length=30)
    lvr_status_description = models.CharField(max_length=30, null=True)

    def __str__(self):
        return self.lvr_status

class LVRLMapprover(models.Model):
    lmapproverlvr = models.ForeignKey(User, related_name='lmapproverlvr', on_delete=models.CASCADE)
    email = models.EmailField(max_length=50, null=True)
    username1 = models.CharField(max_length=30, null=True)
    firstname = models.CharField(max_length=30, null=True)
    lastname = models.CharField(max_length=30, null=True)

    def __str__(self):
        return str(self.username1)

class LVRITstaff(models.Model):
    itapproverlvr = models.ForeignKey(User, related_name='itapproverlvr', on_delete=models.CASCADE)
    email = models.EmailField(max_length=50, null=True)
    username1 = models.CharField(max_length=30, null=True)
    firstname = models.CharField(max_length=30, null=True)
    lastname = models.CharField(max_length=30, null=True)

    def __str__(self):
        return str(self.username1)

class LVRHRapprover(models.Model):
    hrapproverlvr = models.ForeignKey(User, related_name='hrapproverlvr', on_delete=models.CASCADE)
    email = models.EmailField(max_length=50, null=True)
    username1 = models.CharField(max_length=30, null=True)
    firstname = models.CharField(max_length=30, null=True)
    lastname = models.CharField(max_length=30, null=True)

    def __str__(self):
        return str(self.username1)

class LVRPRapprover(models.Model):
    prapproverlvr = models.ForeignKey(User, related_name='prapproverlvr', on_delete=models.CASCADE)
    email = models.EmailField(max_length=50, null=True)
    username1 = models.CharField(max_length=30, null=True)
    firstname = models.CharField(max_length=30, null=True)
    lastname = models.CharField(max_length=30, null=True)

    def __str__(self):
        return str(self.username1)


class LVRModel(models.Model):
    AFC = 'AFC'
    NAFC = 'Non-AFC'

    GRADE_TYPE = (
        (AFC, 'AFC'),
        (NAFC, 'Non-AFC'),

    )

    C1 = 'Linen Non-AFC zero hour Contract'
    C2 = 'Linen Non-AFC Temporary Contract'
    C3 = 'Linen Non-AFC Permanent Contract'
    C4 = 'Linen Non-AFC Permanent Drivers Contract'
    C5 = 'Linen Non-AFC Temp Drivers Contract'

    CONTRACT_TYPE = (
        (C1, 'Linen Non-AFC zero hour Contract'),
        (C2, 'Linen Non-AFC Temporary Contract'),
        (C3, 'Linen Non-AFC Permanent Contract'),
        (C4, 'Linen Non-AFC Permanent Drivers Contract'),
        (C5, 'Linen Non-AFC Temp Drivers Contract'),
    )

    R1 = 'Resignation'
    R2 = 'Dismissal from Conduct'
    R3 = 'Dismissal from Capability'
    R4 = 'Dismissal from Gross Misconduct'
    R5 = 'Settlement Agreement'
    R6 = 'TUPE Transfer'
    R7 = 'Redundancy'
    R8 = 'Some other Substantial Reason'
    R9 = 'Contravention of Law'
    R10 = 'Settlement Agreement'

    REASON = (
        (R1, 'Resignation'),
        (R2, 'Dismissal from Conduct'),
        (R3, 'Dismissal from Capability'),
        (R4, 'Dismissal from Gross Misconduct'),
        (R5, 'Settlement Agreement'),
        (R6, 'TUPE Transfer'),
        (R7, 'Redundancy'),
        (R8, 'Some other Substantial Reason'),
        (R9, 'Contravention of Law'),
        (R10, 'Settlement Agreement'),
    )

    reference = models.SlugField(max_length=10, unique=True)
    author = models.ForeignKey(User, related_name='leaver', on_delete=models.CASCADE)
    date_request = models.DateTimeField(auto_now_add=True)
    status = models.ForeignKey(Status, related_name='leaver', on_delete=models.CASCADE)
    title = models.CharField(max_length=30, null=True)
    first_name = models.CharField(max_length=30, null=True)
    last_name = models.CharField(max_length=30, null=True)
    payroll_number = models.CharField(max_length=30, null=True)
    leaver_date = models.DateField(null=True)
    last_working_day = models.DateField(null=True)
    position_start_date = models.DateField(null=True)
    position_job_title = models.CharField(max_length=30, null=True)
    position_location = models.CharField(max_length=30, null=True)
    position_department = models.CharField(max_length=30, null=True)
    position_costcentre  = models.CharField(max_length=30, null=True)
    position_grade_type = models.CharField(max_length=50, null=True, choices=GRADE_TYPE)
    position_contract_type = models.CharField(max_length=50, null=True, choices=CONTRACT_TYPE)
    holiday_entitlement = models.IntegerField(default=0)
    holiday_taken = models.IntegerField(default=0)
    holiday_owed = models.IntegerField(default=0)
    holiday_deductions = models.IntegerField(default=0)
    redundency_amount = models.IntegerField(default=0)
    redundency_other_payments = models.IntegerField(default=0)
    deductions = models.TextField(max_length=300, null=True)
    reason_for_leaving = models.CharField(max_length=50, null=True, choices=REASON)
    it_hardware = models.TextField(max_length=600, null=True)
    it_services = models.TextField(max_length=600, null=True)
    lvr_comments = models.TextField(max_length=600, null=True)
    line_manager = models.ForeignKey(LVRLMapprover, related_name='leaver', on_delete=models.CASCADE)
    it_staff = models.ForeignKey(LVRITstaff, related_name='leaver', on_delete=models.CASCADE)
    hr_manager = models.ForeignKey(LVRHRapprover, related_name='leaver', on_delete=models.CASCADE)
    payroll_staff = models.ForeignKey(LVRPRapprover, related_name='leaver', on_delete=models.CASCADE)


class Comments(models.Model):
    commentlink = models.ForeignKey(LVRModel, related_name='comment', on_delete=models.CASCADE)
    comment = models.TextField(max_length=500)
    comment_author = models.CharField(max_length=50)

    def __str__(self):
        return self.comment

class Document(models.Model):
    documentlink = models.ForeignKey(LVRModel, related_name='document', on_delete=models.CASCADE)
    description = models.CharField(max_length=255, blank=True)
    document = models.FileField(upload_to='documents/')
    uploaded_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.description

