from django.apps import AppConfig


class HrworkflowappConfig(AppConfig):
    name = 'hrworkflowapp'
