from django.db import models
from django.contrib.auth.models import User, Group
from multiselectfield import MultiSelectField



# Create your models here. .

class Status(models.Model):
    atr_status = models.CharField(max_length=30)
    atr_stage = models.IntegerField(null=True)

    def __str__(self):
        return self.atr_status


class Team(models.Model):
    team_name = models.CharField(max_length=50)
    line_manager = models.CharField(max_length=50, null=True)
    hr_manager = models.CharField(max_length=50, null=True)
    op_director = models.CharField(max_length=50, null=True)
    board_member1 = models.CharField(max_length=50, null=True)
    board_member2 = models.CharField(max_length=50, null=True)

    def __str__(self):
        return self.team_name

class ATRRequester(models.Model):
    username = models.ForeignKey(User, related_name='user', on_delete=models.CASCADE)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.CharField(max_length=50)
    team = models.ForeignKey(Team, related_name='atrrequest', on_delete=models.CASCADE)

    def __str__(self):
        return self.email

class ATRModel(models.Model):
    NONE = 'None'
    Y = 'Y'
    N = 'N'
    HOURLY = 'HOURLY'
    WEEKLY = 'WEEKLY'
    MONTHLY = 'MONTHLY'
    YEARLY = 'YEARLY'
    DRAFT = 'Draft'

    NA = 'None'
    JC = 'Job Centre'
    O = 'Online'
    P = 'Paper'
    CO = 'College'
    AG = 'Agency'

    CAR = 'Car'
    CAR_ALLOWANCE = 'Car Allowance'
    PEN = 'Pension'
    MED = 'Medical'
    OTH = 'Other'

    OPS = 'Operations'
    CDTOPS = 'CDT Operations'
    SOPS = 'Sterile Operations'
    SALES = 'Commercial'
    IT = 'IT'
    MAN = 'Management'
    EXE = 'Executive'
    HR = 'HR'
    QUALITY = 'Quality'
    FIN = 'Finance'
    TRANSPORT = 'Transport'
    AD = 'Administration'
    CUST ='Customer Services'

    LONDON = 'London'
    DERBY = 'Derby'
    DUNSTABLE = 'Dunstable'
    SHEFFIELD = 'Sheffield'
    MOBILE = 'Mobile'
    CDTSOUTH = 'CDT South'
    CDTNORTH = 'CDT North'

    BUDGET_CHOICE = (
        (Y, 'Y'),
        (N, 'N'),
    )

    PAY_PERIOD_CHOICE = (
        (HOURLY, 'Hourly'),
        (WEEKLY, 'Weekly'),
        (MONTHLY, 'Monthly'),
        (YEARLY, 'YEARLY'),
    )

    ADVERT_CHOICE = (
        (NONE,'None'),
        (JC, 'Job Centre'),
        (O, 'Online'),
        (P, 'Paper'),
        (CO, 'College'),
        (AG,'Agency'),

    )

    BENEFITS = (
        (NONE,'None'),
        (CAR, 'Car'),
        (CAR_ALLOWANCE, 'Car Allowance'),
        (PEN, 'Pension'),
        (MED, 'Medical'),
        (OTH, 'Other'),
    )

    DIVISION = (
        (OPS, 'Operations'),
        (CDTOPS, 'CDT Operations'),
        (SOPS, 'Sterile Operations'),
        (SALES, 'Commercial'),
        (IT, 'IT'),
        (MAN, 'Management'),
        (EXE, 'Executive'),
        (HR, 'HR'),
        (QUALITY, 'Quality'),
        (FIN, 'Finance'),
        (TRANSPORT, 'Transport'),
        (AD,'Administration'),
        (CUST, 'Customer Services'),
    )

    LOCATION = (
        (LONDON, 'London'),
        (DERBY, 'Derby'),
        (DUNSTABLE, 'Dunstable'),
        (SHEFFIELD, 'Sheffield'),
        (MOBILE, 'Mobile'),
        (CDTSOUTH, 'CDT South'),
        (CDTNORTH, 'CDT North'),

    )

    NEW = 'New'
    REP = 'Replacement'

    REP_ADD = (
        (NEW,'New'),
        (REP, 'Replacement'),
    )

    PERM = 'Permenant'
    TEMP = 'Temporary'

    CONTRACT = (
        (PERM,'Permenant'),
        (TEMP, 'Temporary'),
    )

    #MODEL ATR
    status = models.ForeignKey(Status, related_name='atrstatus', on_delete=models.CASCADE)
    stage = models.IntegerField(null=True)
    reference = models.SlugField(max_length=10,unique=True)
    author = models.ForeignKey(User, related_name='atrauthor', on_delete=models.CASCADE)
    date_request = models.DateTimeField(auto_now_add=True)
    division = models.CharField(max_length=30, choices=DIVISION)
    location = models.CharField(max_length=50, choices=LOCATION)
    job_title = models.CharField(max_length=30)
    replacement_or_addition = models.CharField(max_length=30, choices=REP_ADD, null=True)
    perm_or_temp = models.CharField(max_length=30, choices=CONTRACT, null=True)
    temp_term = models.IntegerField(default=0)
    nature_app = models.TextField(max_length=500)
    salary = models.DecimalField(max_digits=8, decimal_places=2,default=0.00)
    pay_period = models.CharField(max_length=10, choices=PAY_PERIOD_CHOICE, default='YEARLY')
    contracted_hours = models.IntegerField(default=0)
    benefit_choice = MultiSelectField(choices=BENEFITS, null=True, default='None')
    benefit_details = models.TextField(max_length=300, null=True)
    budget = models.CharField(max_length=2, choices=BUDGET_CHOICE, default='Y')
    out_emp_title = models.CharField(max_length=30, null=True)
    out_emp_salary = models.IntegerField(default=0)
    justification = models.TextField(max_length=500)
    advertising = MultiSelectField(choices=ADVERT_CHOICE, null=True, default='None')
    line_manager = models.CharField(max_length=50,null=True)
    hr_manager = models.CharField(max_length=50,null=True)
    op_director = models.CharField(max_length=50,null=True)
    board_member = models.CharField(max_length=50,null=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.reference

class Comments(models.Model):
    commentlink = models.ForeignKey(ATRModel, related_name='comment', on_delete=models.CASCADE)
    comment = models.TextField(max_length=500)
    comment_author = models.CharField(max_length=50)
    comment_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.comment

class Document(models.Model):
    documentlink = models.ForeignKey(ATRModel, related_name='document', on_delete=models.CASCADE)
    description = models.CharField(max_length=255, blank=True)
    document = models.FileField(upload_to='documents/')
    uploaded_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.description

