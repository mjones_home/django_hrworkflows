from django.shortcuts import render, redirect
from django.views import generic
from django.utils import timezone
from django.contrib.auth.models import User
from django.http import FileResponse
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string

from .models import ATRModel, Status, Comments, Document, Team, ATRRequester
from .forms import ATRForm, CommentsForm, DocumentForm
from .resource import UserResource, TeamResource, ATRResource
from tablib import Dataset

import string, random

# Reference code generator
def id_generator(size=4, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

#Approval and Rejection Section
def approval_system(request, pk):
    approve_obj = ATRModel.objects.get(pk=pk)
    if approve_obj.stage == 1:
        print('1 = Draft')
        sendapprove(request, pk)
        send_email(request, pk)
        return redirect('/atr/request')
    elif approve_obj.stage == 2:
        print('2 = Line Manager')
        atr_comment(request, pk)
        lmapprove(request, pk)
        send_email(request, pk)
        return redirect('/atr/approve')
    elif approve_obj.stage == 3:
        print('3')
        atr_comment(request, pk)
        hrapprove(request, pk)
        send_email(request, pk)
        return redirect('/atr/approve')
    elif approve_obj.stage == 4:
        print('4')
        opdirectorapproval(request, pk)
        send_email(request, pk)
        atr_comment(request, pk)
        return redirect('/atr/approve')
    elif approve_obj.stage == 5:
        print('5')
        boardapprove(request, pk)
        send_email(request, pk)
        atr_comment(request, pk)
        return redirect('/atr/approve')
    elif approve_obj.stage == 6:
        print('6')
    elif approve_obj.stage == 7:
        print('7')
        sendapprove(request, pk)
        send_email(request, pk)
        atr_comment(request, pk)
        return redirect('/atr/request')
    elif approve_obj.stage == 8:
        print('8')

def reject_system(request, pk):
    reject_obj = ATRModel.objects.get(pk=pk)
    if reject_obj.stage == 1:
        print('1')
        return redirect('/atr/approve')
    elif reject_obj.stage == 2:
        print('2')
        rejectatr(request, pk)
        send_reject_email(request, pk)
        atr_comment(request, pk)
        return redirect('/atr/approve')
    elif reject_obj.stage == 3:
        print('3')
        rejectatr(request, pk)
        send_reject_email(request, pk)
        atr_comment(request, pk)
        return redirect('/atr/approve')
    elif reject_obj.stage == 4:
        rejectatr(request, pk)
        send_reject_email(request, pk)
        atr_comment(request, pk)
        return redirect('/atr/approve')
    elif reject_obj.stage == 5:
        rejectatr(request, pk)
        send_reject_email(request, pk)
        atr_comment(request, pk)
        return redirect('/atr/approve')
    elif reject_obj.stage == 6:
        rejectatr(request, pk)
        send_reject_email(request, pk)
        atr_comment(request, pk)
        return redirect('/atr/approve')
    elif reject_obj.stage == 7:
        rejectatr(request, pk)
        send_reject_email(request, pk)
        atr_comment(request, pk)
        return redirect('/atr/approve')
    elif reject_obj.stage == 8:
        rejectatr(request, pk)
        send_reject_email(request, pk)
        atr_comment(request, pk)
        return redirect('/atr/approve')

# View Functions.

def atr_new(request):
    if request.method == "POST":
        user = request.user
        form = ATRForm(request.POST)
        if form.is_valid():
            reference = 'ATR-' + str(id_generator())
            post = form.save(commit=False)
            if post.salary <= 40000:
                post.author = User.objects.get(username=user)
                post.published_date = timezone.now()
                post.reference = reference
                atruser = User.objects.filter(username=user).values('id')
                atruser = atruser[0]
                atruser = atruser['id']
                requester = ATRRequester.objects.get(username=atruser)
                team = Team.objects.get(id=requester.team_id)
                tlm = team.line_manager
                thr = team.hr_manager
                tod = team.op_director
                tb1 = team.board_member1
                post.line_manager = str(tlm)
                post.hr_manager = str(thr)
                post.op_director = str(tod)
                post.board_member = str(tb1)
                post.status = Status.objects.get(atr_status='Draft')
                stage = Status.objects.filter(atr_status='Draft').values('atr_stage')
                stage = stage[0]
                stage = stage['atr_stage']
                post.stage = str(stage)
                post.need_board = False
                post.board_member = None
                post.save()
                return redirect('atrupload', pk=post.id)
            else:
                post.author = User.objects.get(username=user)
                post.published_date = timezone.now()
                post.reference = reference
                atruser = User.objects.filter(username=user).values('id')
                atruser = atruser[0]
                atruser = atruser['id']
                requester = ATRRequester.objects.get(username=atruser)
                team = Team.objects.get(id=requester.team_id)
                tlm = team.line_manager
                thr = team.hr_manager
                tod = team.op_director
                tb1 = team.board_member1
                post.line_manager = str(tlm)
                post.hr_manager = str(thr)
                post.op_director = str(tod)
                post.board_member = str(tb1)
                post.status = Status.objects.get(atr_status='Draft')
                stage = Status.objects.filter(atr_status='Draft').values('atr_stage')
                stage = stage[0]
                stage = stage['atr_stage']
                post.stage = str(stage)
                post.need_board = True
                post.save()
                return redirect('atrupload', pk=post.id)
    else:
        form = ATRForm()
        return render(request, 'hrw/atr_new.html', {'form': form})

def atr_comment(request, pk):
    if request.method == 'POST':
        print("commentformif")
        form = CommentsForm(request.POST)
        if form.is_valid():
            user = request.user
            atrextra = ATRModel.objects.get(id)
            post = form.save(commit=False)
            post.commentlink = atrextra
            post.comment_author = user
            post.save()
            return render(request, 'hrw/atr_home.html')
    else:
        print("commentformelse")
        form = CommentsForm()
        return render(request, 'hrw/atr_comment.html', {'form': form})

def model_form_upload(request, pk):
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            post = form.save(commit=False)
            dlink = ATRModel.objects.filter(pk=pk).values('id')
            dlink = dlink[0]
            dlink = dlink['id']
            post.documentlink = ATRModel.objects.get(pk=pk)
            post.documentlink_id = dlink
            post.save()
            return redirect('atrrequest')
    else:
        form = DocumentForm()
        return render(request, 'hrworkflowapp/model_form_upload.html', {'form': form})


def draftapproval(request, pk):
    draftapproval = ATRModel.objects.get(pk=pk)
    if draftapproval:
        draftapproval.status = Status.objects.get(atr_status='Draft')
        draftapproval.stage = 1
        draftapproval.save()
        return

def sendapprove(request, pk):
    sendapprove = ATRModel.objects.get(pk=pk)
    if sendapprove:
        user = request.user
        sendapprove.status = Status.objects.get(atr_status='Line Manager Approval')
        sendapprove.stage = 2
        sendapprove.save()
        return

def lmapprove(request, pk):
    lmapprove = ATRModel.objects.get(pk=pk)
    if lmapprove:
        user = request.user
        lmapprove.status = Status.objects.get(atr_status='HR Approval')
        lmapprove.stage = 3
        lmapprove.save()
        return


def hrapprove(request, pk):
    hrapprove = ATRModel.objects.get(pk=pk)
    if hrapprove:
        hrapprove.status = Status.objects.get(atr_status='Operational Director Approval')
        hrapprove.stage = 4
        hrapprove.save()
        return


def opdirectorapproval(request, pk):
    opdirectorapproval = ATRModel.objects.get(pk=pk)
    if opdirectorapproval:
        if opdirectorapproval.salary >= 40000:
            opdirectorapproval.status = Status.objects.get(atr_status='Board Approval')
            opdirectorapproval.stage = 5
            opdirectorapproval.save()
            return redirect('/atr/approve')
        else:
            opdirectorapproval.status = Status.objects.get(atr_status='Complete')
            opdirectorapproval.stage = 6
            opdirectorapproval.save()
        return

def boardapprove(request, pk):
    boardapprove = ATRModel.objects.get(pk=pk)
    if boardapprove:
        boardapprove.status = Status.objects.get(atr_status='Complete')
        boardapprove.stage = 6
        boardapprove.save()
        return

def completeapproval(request, pk):
    completeapproval = ATRModel.objects.get(pk=pk)
    if completeapproval:
        completeapproval.status = Status.objects.get(atr_status='Complete')
        completeapproval.stage = 6
        completeapproval.save()
        return

def rejectatr(request, pk):
    rejectatr = ATRModel.objects.get(pk=pk)
    if rejectatr:
        rejectatr.status = Status.objects.get(atr_status='Rejected')
        rejectatr.stage = 7
        rejectatr.save()
        return

def download(request, pk):
    document = Document.objects.filter(id=pk).values('document')
    document = document[0]
    document = document['document']
    document = str('media/' + document)
    response = FileResponse(open(document, 'rb'))
    return response

def delete_obj(request, pk):
    delete_object = ATRModel.objects.get(pk=pk)
    if delete_object.status_id == 1:
        delete_object.delete()
        return redirect('/atr/request')
    elif delete_object.status_id == 7:
        delete_object.delete()
        return redirect('/atr/request')
    else:
        print("Incorrect Status")
        return redirect('/atr/request')

def simple_upload_user(request):
    if request.method == 'POST':
        user_resource = UserResource()
        dataset = Dataset()
        new_persons = request.FILES['myfile']
        #new_persons = request.FILES

        imported_data = dataset.load(new_persons.read().decode('utf-8'),format='csv')
        result = user_resource.import_data(dataset, dry_run=True)  # Test the data import

        if not result.has_errors():
            user_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'hrworkflowapp/import_user.html')

def simple_upload_team(request):
    if request.method == 'POST':
        team_resource = TeamResource()
        dataset = Dataset()
        new_team = request.FILES['myfile']

        imported_data = dataset.load(new_team.read().decode('utf-8'),format='csv')
        result = team_resource.import_data(dataset, dry_run=True)  # Test the data import

        if not result.has_errors():
            team_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'hrworkflowapp/import_team.html')

def simple_upload_atrr(request):
    if request.method == 'POST':
        atrr_resource = ATRResource()
        dataset = Dataset()
        new_atrr = request.FILES['myfile']

        imported_data = dataset.load(new_atrr.read().decode('utf-8'),format='csv')
        result = atrr_resource.import_data(dataset, dry_run=True)  # Test the data import

        if not result.has_errors():
            atrr_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'hrworkflowapp/import.html')

# Class Based Views.
class ATRIndexView(generic.TemplateView):
    template_name = 'hrw/atr_home.html'
    context_object_name = 'atr_list'

    def get_queryset(self):
        return

class ATRRequestView(generic.ListView):
    template_name = 'hrw/atr_requests.html'
    context_object_name = 'atr_list'
    paginate_by = 10

    def get_queryset(self):
        user = self.request.user
        authors = User.objects.get(username=user)
        context = ATRModel.objects.order_by('-date_request').filter(author=authors)
        return context

class ATRDetailView(generic.DetailView):
    model = ATRModel
    template_name = 'hrw/atr_detail.html'
    context_object_name = 'atr'

    def get_queryset(self):
        user = self.request.user
        authors = User.objects.get(username=user)
        try:
            atrcheck = ATRModel.objects.filter(author=user).values('author')
            atrcheck = atrcheck[0]
            atrcheck = atrcheck['author']

            people = User.objects.get(id=atrcheck)
            if user == people:
                print ("match")
                return ATRModel.objects.order_by('-date_request').filter(author=authors)
            else:
                print("else")
                return
        except:
            return redirect('/atr/norequests')

class ATRApproveDetailView(generic.DetailView):
    model = ATRModel
    template_name = 'hrw/atr_approvedetail.html'
    #template_name = 'hrw/atr_detail.html'
    context_object_name = 'atr'

    def get_queryset(self):
        user = self.request.user
        print(user)

        lm = ATRModel.objects.filter(line_manager=user).filter(stage=2)
        print(lm)
        hr = ATRModel.objects.filter(hr_manager=user).filter(stage=3)
        print(hr)
        do = ATRModel.objects.filter(op_director=user).filter(stage=4)
        print(do)
        board = ATRModel.objects.filter(board_member=user).filter(stage=5)
        print(board)

        context = lm | hr | do | board
        return context.order_by('-date_request')


class ATRApproveView(generic.ListView):
    template_name = 'hrw/atr_approve.html'
    context_object_name = 'atr_list'

    def get_queryset(self):
        user = self.request.user
        print(user)

        lm = ATRModel.objects.filter(line_manager=user).filter(stage=2)
        print(lm)
        hr = ATRModel.objects.filter(hr_manager=user).filter(stage=3)
        print(hr)
        do = ATRModel.objects.filter(op_director=user).filter(stage=4)
        print(do)
        board = ATRModel.objects.filter(board_member=user).filter(stage=5)
        print(board)
        context = lm | hr | do | board
        return context.order_by('-date_request')


class ATRUpdateView(generic.UpdateView):
    model = ATRModel
    fields = (
        'division',
        'location',
        'job_title',
        'replacement_or_addition',
        'perm_or_temp',
        'nature_app',
        'salary',
        'pay_period',
        'contracted_hours',
        'benefit_choice',
        'benefit_details',
        'budget',
        'out_emp_title',
        'out_emp_salary',
        'justification',
        'advertising',
    )
    template_name_suffix = '_update_form'

    def get_success_url(self):

        """Return the URL to redirect to after processing a valid form."""
        return str(self.success_url)  # success_url may be lazy

class ATRHRView(generic.ListView):
    template_name = 'hrw/atr_hrview.html'
    context_object_name = 'atr_list'
    paginate_by = 10

    def get_queryset(self):
        return ATRModel.objects.order_by('-date_request')

class ATRHRDetailView(generic.DetailView):
    model = ATRModel
    template_name = 'hrw/atr_hrdetail.html'
    context_object_name = 'atr'

    def get_queryset(self):
        return ATRModel.objects.order_by('-date_request')

class ATRComments(generic.ListView):
    model = Comments
    template_name = 'hrw/comment_details.html'
    context_object_name = 'com_list'


    def get_queryset(self, *args, **kwarg):
        pk = self.kwargs.get('pk')
        atr = ATRModel.objects.filter(id=pk).values('id')
        atr = atr[0]
        atr = atr['id']
        if atr:
            try:
                comments = Comments.objects.order_by('comment').filter(commentlink_id=atr)
                return comments
            except:
                #Comments.objects.order_by('comment').filter(commentlink=atr)
                pass

class ATRDocumentsView(generic.ListView):
    model = Document
    template_name = 'hrw/document_details.html'
    context_object_name = 'doc'


    def get_queryset(self, *args, **kwarg):
        pk = self.kwargs.get('pk')
        atr = ATRModel.objects.filter(id=pk).values('id')
        atr = atr[0]
        atr = atr['id']
        if atr:
            try:
                document = Document.objects.get(documentlink_id=pk)
                return document
            except:
                document = 0
                return document

def send_reject_email(request, pk):
    atrextra = ATRModel.objects.get(id=pk)
    subject = str(atrextra.reference + " " + "-" + " " + "Requires" + " " + str(atrextra.status) + " " + "for" + " " +
                  str(atrextra.job_title))
    merge_data = {
            'LINK': pk, 'STATUS': atrextra.status
    }

    text_body = render_to_string("hrw/message_body.txt", merge_data)
    html_body = render_to_string("hrw/reject_email.html", merge_data)
    get_email = User.objects.get(username=atrextra.author)
    to_user = str(get_email.email)

    msg = EmailMultiAlternatives(subject=subject, from_email="HRWorkflows@synergylms.co.uk",
                                     to=[to_user], body=text_body)
    msg.attach_alternative(html_body, "text/html")
    msg.send()
    return

def send_email(request, pk):
    atrextra = ATRModel.objects.get(id=pk)
    subject = str(
        atrextra.reference + " " + "-" + " " + "Requires" + " " + str(atrextra.status) + " " + "for" + " " + str(
            atrextra.job_title))
    merge_data = {
            'LINK': pk, 'STATUS': atrextra.status
    }


    text_body = render_to_string("hrw/message_body.txt", merge_data)
    html_body = render_to_string("hrw/email.html", merge_data)

    if atrextra.stage == 1:
        get_email = User.objects.get(username=atrextra.author)
        to_user = str(get_email.email)
        msg = EmailMultiAlternatives(subject=subject, from_email="HRWorkflows@synergylms.co.uk",
                                     to=[to_user], body=text_body)
        msg.attach_alternative(html_body, "text/html")
        msg.send()
        return
    elif atrextra.stage == 2:
        get_email = User.objects.get(username=atrextra.line_manager)
        to_user = str(get_email.email)
        msg = EmailMultiAlternatives(subject=subject, from_email="HRWorkflows@synergylms.co.uk",
                                     to=[to_user], body=text_body)
        msg.attach_alternative(html_body, "text/html")
        msg.send()
        return
    elif atrextra.stage == 3:
        get_email = User.objects.get(username=atrextra.hr_manager)
        to_user = str(get_email.email)
        msg = EmailMultiAlternatives(subject=subject, from_email="HRWorkflows@synergylms.co.uk",
                                     to=[to_user], body=text_body)
        msg.attach_alternative(html_body, "text/html")
        msg.send()
        return
    elif atrextra.stage == 4:
        get_email = User.objects.get(username=atrextra.op_director)
        to_user = str(get_email.email)
        msg = EmailMultiAlternatives(subject=subject, from_email="HRWorkflows@synergylms.co.uk",
                                     to=[to_user], body=text_body)
        msg.attach_alternative(html_body, "text/html")
        msg.send()
        return
    elif atrextra.stage == 5:
        get_email = User.objects.get(username=atrextra.board_member)
        to_user = str(get_email.email)
        msg = EmailMultiAlternatives(subject=subject, from_email="HRWorkflows@synergylms.co.uk",
                                     to=[to_user], body=text_body)
        msg.attach_alternative(html_body, "text/html")
        msg.send()
        return
    elif atrextra.stage == 6:
        get_email = User.objects.get(username=atrextra.author)
        to_user = str(get_email.email)
        msg = EmailMultiAlternatives(subject=subject, from_email="HRWorkflows@synergylms.co.uk",
                                     to=[to_user], body=text_body)
        msg.attach_alternative(html_body, "text/html")
        msg.send()
        return
    elif atrextra.stage == 7:
        get_email = User.objects.get(username=atrextra.author)
        to_user = str(get_email.email)
        msg = EmailMultiAlternatives(subject=subject, from_email="HRWorkflows@synergylms.co.uk",
                                     to=[to_user], body=text_body)
        msg.attach_alternative(html_body, "text/html")
        msg.send()
        return


class NOATRIndexView(generic.TemplateView):
    template_name = 'hrw/atr_norequests.html'
    context_object_name = 'atr_list'

    def get_queryset(self):
        return

class ATRProfileView(generic.ListView):
    template_name = 'hrw/atr_profile.html'
    context_object_name = 'atr_list'

    def get_queryset(self):
        user = self.request.user
        print(user)
        atruser = User.objects.filter(username=user).values('id')
        atruser = atruser[0]
        atruser = atruser['id']
        try:
            requester = ATRRequester.objects.get(username=atruser)
            team = Team.objects.get(id=requester.team_id)
            print(team)
            print(requester.team_id)
            return team

        except:
            return redirect('atr/')



