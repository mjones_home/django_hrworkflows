from django.forms import ModelForm
from django.utils.translation import gettext_lazy as _

from .models import ATRModel, Comments, Document

class ATRForm(ModelForm):

    class Meta:
        model = ATRModel
        fields = '__all__'
        exclude = ('reference',
                   'status',
                   'author',
                   'stage',
                   'need_board',
                   'line_manager',
                   'hr_manager',
                   'board_member',
                   'op_director'
                   )
        labels = {
            'nature_app' : _('Nature of Application'),
            'out_emp_title': _('Outgoing Employee Title'),
            'out_emp_salary': _('Outgoing Employee Salary'),
            'replacement_or_addition': _('Choose whether new employee or replacement'),
            'temp_term': _('Temp Period'),
                  }

class CommentsForm(ModelForm):
    class Meta:
        model = Comments
        fields = ('comment',)

class DocumentForm(ModelForm):
    class Meta:
        model = Document
        fields = ('description', 'document', )
