from django.contrib import admin
from hrworkflowapp.models import ATRModel, Status, Comments, Document, Team, ATRRequester
from import_export.admin import ImportExportModelAdmin
# Register your models here.

class ATRAdmin(admin.ModelAdmin):
    list_display = ('reference', 'job_title','status','author')
    list_filter = ['job_title']
    search_fields = ['job_title']

admin.site.register(ATRModel, ATRAdmin)

class StatusAdmin(admin.ModelAdmin):
    list_display = ('atr_status', 'id')
    list_filter = ['atr_status']
    search_fields = ['atr_status']

admin.site.register(Status, StatusAdmin)

class CommentsAdmin(admin.ModelAdmin):
    list_display = ['comment']
    list_filter = ['comment']
    search_fields = ['comment']

admin.site.register(Comments, CommentsAdmin)

class DocumentsAdmin(admin.ModelAdmin):
    list_display = ['description']
    list_filter = ['description']
    search_fields = ['description']

admin.site.register(Document, DocumentsAdmin)

class TeamAdmin(ImportExportModelAdmin):
    list_display = ['team_name','line_manager','hr_manager','op_director','board_member1','board_member2']
    list_filter = ['team_name']
    search_fields = ['team_name']

admin.site.register(Team, TeamAdmin)

class ATRRequesterAdmin(admin.ModelAdmin):
    list_display = ['username','team']
    list_filter = ['username']
    search_fields = ['username']

admin.site.register(ATRRequester, ATRRequesterAdmin)


class UserBulkAdmin(ImportExportModelAdmin):
    pass

