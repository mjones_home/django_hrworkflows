from import_export import fields, resources
from .models import User, Team, ATRRequester
from import_export.widgets import ForeignKeyWidget

class UserResource(resources.ModelResource):
    class Meta:
        model = User
        fields = ('username','first_name','last_name','email','id')

class TeamResource(resources.ModelResource):

        class Meta:
            model = Team
            fields = ('team_name','line_manager','hr_manager','op_director','board_member1','board_member2','id')


class ATRResource(resources.ModelResource):
    username = fields.Field(
        column_name='username',
        attribute='username',
        widget=ForeignKeyWidget(User, 'username'))

    team = fields.Field(
        column_name='team',
        attribute='team',
        widget=ForeignKeyWidget(Team, 'team_name'))

    class Meta:
        model = ATRRequester
        fields = ('username','first_name','last_name','email','team','id')
