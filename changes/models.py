from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User, Group

# Create your models here.
class Status(models.Model):
    chg_status = models.CharField(max_length=30)
    chg_status_description = models.CharField(max_length=30, null=True)

    def __str__(self):
        return self.chg_status

class CHGLMapprover(models.Model):
    lmapproverchg = models.ForeignKey(User, related_name='lmapproverchg', on_delete=models.CASCADE)
    email = models.EmailField(max_length=50, null=True)
    username1 = models.CharField(max_length=30, null=True)
    firstname = models.CharField(max_length=30, null=True)
    lastname = models.CharField(max_length=30, null=True)

    def __str__(self):
        return str(self.username1)

class CHGHRapprover(models.Model):
    hrapproverchg = models.ForeignKey(User, related_name='hrapproverchg', on_delete=models.CASCADE)
    email = models.EmailField(max_length=50, null=True)
    username1 = models.CharField(max_length=30, null=True)
    firstname = models.CharField(max_length=30, null=True)
    lastname = models.CharField(max_length=30, null=True)

    def __str__(self):
        return str(self.username1)

class CHGPRapprover(models.Model):
    prapproverchg = models.ForeignKey(User, related_name='prapproverchg', on_delete=models.CASCADE)
    email = models.EmailField(max_length=50, null=True)
    username1 = models.CharField(max_length=30, null=True)
    firstname = models.CharField(max_length=30, null=True)
    lastname = models.CharField(max_length=30, null=True)

    def __str__(self):
        return str(self.username1)


class CHGModel(models.Model):
    AFC = 'AFC'
    NAFC = 'Non-AFC'

    GRADE_TYPE = (
        (AFC, 'AFC'),
        (NAFC, 'Non-AFC'),

    )

    C1 = 'Linen Non-AFC zero hour Contract'
    C2 = 'Linen Non-AFC Temporary Contract'
    C3 = 'Linen Non-AFC Permanent Contract'
    C4 = 'Linen Non-AFC Permanent Drivers Contract'
    C5 = 'Linen Non-AFC Temp Drivers Contract'

    CONTRACT_TYPE = (
        (C1, 'Linen Non-AFC zero hour Contract'),
        (C2, 'Linen Non-AFC Temporary Contract'),
        (C3, 'Linen Non-AFC Permanent Contract'),
        (C4, 'Linen Non-AFC Permanent Drivers Contract'),
        (C5, 'Linen Non-AFC Temp Drivers Contract'),
    )


    reference = models.SlugField(max_length=10, unique=True)
    author = models.ForeignKey(User, related_name='changes', on_delete=models.CASCADE)
    date_request = models.DateTimeField(auto_now_add=True)
    status = models.ForeignKey(Status, related_name='changes', on_delete=models.CASCADE)
    title = models.CharField(max_length=30, null=True)
    first_name = models.CharField(max_length=30, null=True)
    last_name = models.CharField(max_length=30, null=True)
    payroll_number = models.CharField(max_length=30, null=True)
    position_job_title = models.CharField(max_length=30, null=True)
    position_location = models.CharField(max_length=30, null=True)
    position_department = models.CharField(max_length=30, null=True)
    position_costcentre  = models.CharField(max_length=30, null=True)
    position_grade_type = models.CharField(max_length=50, null=True, choices=GRADE_TYPE)
    position_contract_type = models.CharField(max_length=50, null=True, choices=CONTRACT_TYPE)
    current_location = models.CharField(max_length=30, null=True)
    current_costcentre = models.CharField(max_length=30, null=True)
    current_job_title = models.CharField(max_length=30, null=True)
    current_line_manager = models.CharField(max_length=50, null=True)
    current_contract_type = models.CharField(max_length=50, null=True, choices=CONTRACT_TYPE)
    current_contract_review_date = models.DateTimeField(null=True)
    current_total_weekly_hours = models.IntegerField(null=True)
    current_normal_working_days = models.IntegerField(null=True)
    current_AFC_band = models.CharField(max_length=50, null=True)
    current_AFC_point = models.CharField(max_length=50, null=True)
    current_AFC_increment_date = models.DateTimeField(null=True)
    current_company_car_band = models.CharField(max_length=50, null=True)
    current_car_allowance = models.IntegerField(null=True)
    current_private_health = models.IntegerField(null=True)
    current_pension_employer = models.IntegerField(null=True)
    current_pension_employee = models.IntegerField(null=True)
    current_fixed_allowance = models.IntegerField(null=True)
    current_fixed_deduction = models.IntegerField(null=True)
    current_bonus_percentage = models.IntegerField(null=True)
    current_personal_details = models.TextField(max_length=600, null=True)
    new_location = models.CharField(max_length=30, null=True)
    new_costcentre = models.CharField(max_length=30, null=True)
    new_job_title = models.CharField(max_length=30, null=True)
    new_line_manager = models.CharField(max_length=50, null=True)
    new_contract_type = models.CharField(max_length=50, null=True, choices=CONTRACT_TYPE)
    new_contract_review_date = models.DateTimeField(null=True)
    new_total_weekly_hours = models.IntegerField(null=True)
    new_normal_working_days = models.IntegerField(null=True)
    new_AFC_band = models.CharField(max_length=50, null=True)
    new_AFC_point = models.CharField(max_length=50, null=True)
    new_AFC_increment_date = models.DateTimeField(null=True)
    new_company_car_band = models.CharField(max_length=50, null=True)
    new_car_allowance = models.IntegerField(null=True)
    new_private_health = models.IntegerField(null=True)
    new_pension_employer = models.IntegerField(null=True)
    new_pension_employee = models.IntegerField(null=True)
    new_fixed_allowance = models.IntegerField(null=True)
    new_fixed_deduction = models.IntegerField(null=True)
    new_bonus_percentage = models.IntegerField(null=True)
    new_personal_details = models.TextField(max_length=600, null=True)
    effective_change_date = models.DateTimeField(null=True)
    current_work_pattern = models.TextField(max_length=600, null=True)
    chg_comments = models.TextField(max_length=600, null=True)
    line_manager = models.ForeignKey(CHGLMapprover, related_name='changes', on_delete=models.CASCADE)
    hr_manager = models.ForeignKey(CHGHRapprover, related_name='changes', on_delete=models.CASCADE)
    payroll_staff = models.ForeignKey(CHGPRapprover, related_name='changes', on_delete=models.CASCADE)


class Comments(models.Model):
    commentlink = models.ForeignKey(CHGModel, related_name='comment', on_delete=models.CASCADE)
    comment = models.TextField(max_length=500)
    comment_author = models.CharField(max_length=50)

    def __str__(self):
        return self.comment

class Document(models.Model):
    documentlink = models.ForeignKey(CHGModel, related_name='document', on_delete=models.CASCADE)
    description = models.CharField(max_length=255, blank=True)
    document = models.FileField(upload_to='documents/')
    uploaded_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.description
