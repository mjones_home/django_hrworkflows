from django.shortcuts import render, redirect
from django.views import generic, View
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.models import User, Group
from django.db.models import Q
from django.core.paginator import Paginator
from django.http import FileResponse
from django.core.mail import EmailMultiAlternatives, send_mail,EmailMessage

from .models import CHGModel,CHGHRapprover,CHGLMapprover,CHGPRapprover, Status, Comments, Document
from .forms import CHGForm, CommentsForm, DocumentForm

import string, random

# Reference code generator
def id_generator(size=4, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

# Create your views here.


# Functional Views
def chg_new(request):
    if request.method == "POST":
        user = request.user
        print(user)
        form = CHGForm(request.POST)
        if form.is_valid():
            reference = 'CHG-' + str(id_generator())
            print(reference)
            post = form.save(commit=False)
            post.author = User.objects.get(username=user)
            post.published_date = timezone.now()
            post.reference = reference
            post.status = Status.objects.get(chg_status='Draft')
            print(post.status)
            #upload = model_form_upload(request, pk=post.id)
            post.save()
            return redirect('chghome')

    else:
        form = CHGForm()
        return render(request, 'changes/chg_new.html', {'form': form})


# Class Views
class CHGIndex(generic.TemplateView):
    template_name = 'changes/chg_home.html'
    context_object_name = 'chg_list'

    def get_queryset(self):
        return


class CHGRequestView(generic.ListView):
    template_name = 'changes/chg_requests.html'
    context_object_name = 'chg_list'

    def get_queryset(self):
        user = self.request.user
        print(user)
        authors = User.objects.get(username=user)
        print(authors.id)
        author = authors.id
        return CHGModel.objects.order_by('-date_request').filter(author_id=author)

class CHGApproveView(generic.ListView):
    template_name = 'changes/chg_approve.html'
    context_object_name = 'chg_list'

    def get_queryset(self):
        user = self.request.user
        userid = User.objects.filter(username=user).values('id')
        userid = userid[0]
        userid = userid['id']
        userid = str(userid)
        print("userid" + userid)



        try:
            pr_test = CHGPRapprover.objects.filter(prapproverchg_id=userid).values('prapproverchg_id')
            print(pr_test)
            pr_test = pr_test[0]
            pr_test = pr_test['prapproverchg_id']
            pr_test = str(pr_test)
        except IndexError:
            pr_test = None

        try:
            hr_test = CHGHRapprover.objects.filter(hrapproverchg_id=userid).values('hrapproverchg_id')
            print(hr_test)
            hr_test = hr_test[0]
            hr_test = hr_test['hrapproverchg_id']
            hr_test = str(hr_test)
        except IndexError:
            hr_test = None

        try:
            auth_test = CHGLMapprover.objects.filter(lmapproverchg_id=userid).values('lmapproverchg_id')
            print(auth_test)
            auth_test = auth_test[0]
            auth_test = auth_test['lmapproverchg_id']
            auth_test = str(auth_test)
            print(auth_test)
        except IndexError:
            print('auth_test_except')


        if pr_test == userid:
            status = CHGModel.objects.filter(status__chg_status__exact='HR Approved')
            return status.order_by('-date_request')
        elif hr_test == userid:
            status = CHGModel.objects.filter(status__chg_status__exact='HR Approval')
            return status.order_by('-date_request')
        elif auth_test == userid:
            print('Matcb Auth test')
            status = CHGModel.objects.filter(status__chg_status__exact='Send for Approval')
            print(status)
            return status.order_by('-date_request')


class CHGHRView(generic.ListView):
    template_name = 'changes/chg_hrview.html'
    context_object_name = 'chg_list'
    paginate_by = 10

    def get_queryset(self):
        return CHGModel.objects.order_by('-date_request')